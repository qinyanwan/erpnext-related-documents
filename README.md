# ERPNext相关文档


### 资源下载

#### 虚拟机OVF镜像

ERPNEXT v15 OVF镜像，可用VMWare WorkStation、VMWare Fusion(Intel芯片)、vSphere/EXSI、Microsoft Hyper-V（通过System Center Virtual Machine Manager)、Oracle VirtualBox虚拟机导入使用。

##### 虚拟机镜像下载:

https://url43.ctfile.com/f/62348743-1433643665-14e42b?p=1423  (访问密码: 1423)

##### 虚拟机镜像使用说明下载：

https://url43.ctfile.com/f/62348743-1433643668-af6d9b?p=1423  (访问密码: 1423)


ERPNext version develop (v16) 虚拟机镜像下载(分卷压缩，请下载完三个镜像后再一起解压):

ERPNext dev.part1.rar:

https://url43.ctfile.com/f/62348743-1439970536-d0ffa7?p=1423 (访问密码: 1423)

ERPNext dev.part2.rar:

https://url43.ctfile.com/f/62348743-1439985050-5a2268?p=1423 (访问密码: 1423)

ERPNext dev.part3.rar:

https://url43.ctfile.com/f/62348743-1439987060-1a157b?p=1423 (访问密码: 1423)

 
### 安装

存放安装文档等相关资料
安装视频可参考

西瓜视频：https://www.ixigua.com/7227999770369851944

B站：https://www.bilibili.com/video/BV15o4y1x7Sk/?spm_id_from=333.1007.top_right_bar_window_history.content.click


1. 官方每次大小版本更新，安装所需的环境都可能会有所变化，这里保留有新旧版本安装文档，如安装过程中遇到报错，可多参考几个版本来处理。
2. 如需要安装或教学服务，可加微信：prepress，费用详谈（需注明来意）。


### 重磅推荐

1.  建议安装汉化及中国本地化（开箱即用） ，仓库地址：https://gitee.com/yuzelin
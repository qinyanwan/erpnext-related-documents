# Frappe/ERPNext v14升级v15



1. 备份（数据库、快照、镜像）

2. 把node.js从16升级到18

   ```
   sudo apt update
   sudo npm install -g n
   sudo n 18
   ```

3. 进入bench工作台目录，将v14版升级到最新

   ```
   bench update --reset
   ```

4. 将bench 版本升级到最新

   ```
   sudo pip3 install  -U frappe-bench
   ```

5. 切换到v15

   ```
   bench switch-to-branch version-15 frappe erpnext --upgrade
   ```

   

6. 依次执行以下命令

   ```
   bench update --patch --no-backup
   bench update --requirements
   bench migrate
   bench restart
   ```

7. 若提示依赖有问题，站点访问报503，可执行

   ```
   bench update --requirements --no-backup
   ```


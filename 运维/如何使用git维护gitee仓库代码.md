# 如何初始化一个仓库并上传代码

1. gitee创建仓库并进行初始化

2. 复制仓库地址

3. 终端中进入代码文件夹。`git init` 命令用于在一个目录中创建一个新的Git仓库，将其转变为Git仓库。这意味着在该目录中开始进行版本控制，可以开始跟踪文件的更改、提交、创建分支等操作。执行 `git init` 命令后，Git会在当前目录下创建一个名为 `.git` 的子目录，用来存储仓库的相关信息和版本控制所需的数据。这样就可以开始使用Git来管理项目的版本控制了

   ```
   git init
   ```

4. `git remote add origin` 这个命令是用来将远程仓库的 URL 添加到本地 Git 仓库中的。在这个命令中，`origin` 是给远程仓库起的名称，通常用来代表远程仓库的主要来源。一旦执行了 `git remote add origin <远程仓库的URL>` 这个命令，就会将远程仓库的 URL 添加到本地 Git 仓库中，并且将这个远程仓库标记为 `origin`。

   这个命令通常是在创建一个新的本地仓库并且需要将其连接到远程仓库时使用的。通过添加远程仓库的 URL，你就可以将本地仓库与远程仓库关联起来，方便进行推送（push）和拉取（pull）操作。

   ```
   git remote add origin + 仓库地址
   ```

5. `git pull origin master` 是一个Git命令，它的作用是从远程仓库（通常是一个名为"origin"的远程仓库）拉取（fetch）最新的更改，并将其合并（merge）到本地的当前分支（通常是"master"分支）。换句话说，这个命令会将远程仓库的最新更改同步到你的本地仓库，以确保你的本地分支是最新的。

   ```
   git pull origin mster
   ```

6. `git add .` 命令用于将当前目录下所有修改过的文件添加到暂存区（Staging Area），准备提交到版本控制系统（如Git）中。这个命令会将所有新建、修改和删除的文件都添加到暂存区，使这些更改可以被包含在下一次的提交中。

   ```
   git add .
   ```

7. `git commit -m`命令用于将暂存区中的更改保存到本地仓库，并附带一条提交信息。 `-m`标志后面跟着的是您对此次提交的简短描述。这个描述对于其他开发人员来说应该是清晰易懂的，以便他们了解这次提交所做的更改。提交信息对于代码版本控制和项目协作非常重要。

   ```
   git commit -m "提交的备注"
   ```

8. `git push origin` 是一个Git命令，用于将本地代码库中的更改推送（push）到远程代码库（通常是在服务器上）。在这个命令中，`origin` 是远程代码库的名称，通常默认是指向你的远程仓库的名称，但也可以是其他名字，取决于你在本地仓库中设置的远程仓库名称。

   当你在本地进行了一些代码修改并且想要将这些修改同步到远程仓库时，你可以使用 `git push origin` 命令将这些更改推送到远程仓库。这样其他合作者或者你在其他机器上的工作副本就可以看到这些更新了。

   ```
   git push origin
   ```

9. 本地代码更改后，要更新到代码库，从第6步开始就可以了。



# 怎样在仓库里创建一个分支

要在Git仓库中创建一个新的分支，可以按照以下步骤进行：

1. 确保你当前在 `develop` 分支上工作。可以通过 `git checkout develop` 命令切换到 `develop` 分支。

2. 使用以下命令创建并切换到新的分支，假设新分支名为 `feature-branch`：


```
 git checkout -b feature-branch
```


   这个命令相当于两个步骤的组合：`git branch feature-branch`（创建新分支）和 `git checkout feature-branch`（切换到新分支）。

3. 现在你已经成功创建了一个名为 `feature-branch` 的新分支，并且切换到了这个新分支上。在这个分支上进行修改并提交更改。

4. 如果需要将这个新分支推送到远程仓库，可以使用以下命令：


```
git push origin feature-branch
```

这样就成功地在Git仓库中创建了一个新的分支并切换到这个分支上。



# 怎样从develop分支合并到master分支？

要将 `develop` 分支的更改合并到 `master` 分支，可以按照以下步骤进行：

1. 确保你当前在 `master` 分支上。如果不在 `master` 分支上，可以使用以下命令切换到 `master` 分支：


```
git checkout master
```




2. 然后，从 `develop` 分支合并到 `master` 分支，可以使用以下命令：


```
 git merge develop
```

   这个命令会将 `develop` 分支的更改合并到当前所在的 `master` 分支。



3. 解决任何可能出现的合并冲突（如果有的话）。当Git无法自动合并更改时，会发生合并冲突，需要手动解决冲突后再进行提交。

4. 最后，进行提交以完成合并：


```
git commit -m "Merge develop into master"
```


   这将创建一个合并提交，将 `develop` 分支的更改合并到 `master` 分支中。

5. 如果需要，可以将 `master` 分支推送到远程仓库：


```
git push origin master
```


这样就成功将 `develop` 分支的更改合并到了 `master` 分支中。
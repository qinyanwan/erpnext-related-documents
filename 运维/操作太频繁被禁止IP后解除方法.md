先确定谁被关住了，第二步再捞人

```bash
sudo fail2ban-client status nginx-proxy
```


```bash
sudo fail2ban-client set nginx-proxy unbanip <ip address>
```

在以下配置里将maxretry改大一点，bantime改小一些（默认600）

```
sudo nano /etc/fail2ban/jail.d/nginx-proxy.conf
```

改完重启一下fail2ban

```bash
sudo systemctl restart fail2ban
```


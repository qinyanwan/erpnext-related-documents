# ERPNEXT数据备份与还原



## 备份

1. 仅备份数据库

   ```
   bench --site all backup
   ```

   

2. 备份所有文件（包括数据库、公共、私有文件以及站点配置文件）

   ```
   bench --site all backup --with-files
   ```



## 还原

1. 仅还原数据库

   ```
   bench restore {bak.sql.gz}
   ```

   

2. 还原所有文件（不包括站点配置文件）

   ```
   bench restore {bak.sql.gz} --with-public-files {*.tar} --with-private-files {.tar}
   ```



> [!NOTE]
>
> 1. { }号里的请对应换成文件路径，示例：`bench restore /home/frappe/erpnext.backup.sql.gz`
> 2. 还原后需要执行 bench migrate 命令
> 3. --site 参数视需要而定，只有一个站点可以不加


1. 安装docker

   ```
   curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
   ```

2. 创建一个名为`docker-compose.yml`的文件，并将以下内容添加到文件中

   ```
   version: '3'
   services:
     metabase:
       image: metabase/metabase
       restart: always
       ports:
         - "3000:3000"
       volumes:
         - ./metabase-data:/metabase-data
   ```

3. 确保当前用户已经添加到docker组中，这样他们就可以访问Docker引擎的Unix套接字。您可以使用以下命令将当前用户添加到docker组中：

   ```
   sudo usermod -aG docker $USER
   ```

   

4. 启动docker

   ```
   sudo docker-compose up -d
   ```



以上经虚拟机测试验证过，不会跟已经装有ERPNEXT的有冲突。